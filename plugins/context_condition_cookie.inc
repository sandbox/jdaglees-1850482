<?php
/**
 * @file
 * Provide context_condition class implmentation for cookie.
 */

/**
 * Condition class implementation.
 */
class ContextCookieCondition extends context_condition {

  /**
   * Provide list of cookie variable keys as requested on settings page.
   */
  public function condition_values() {
    return variable_get('context_cookie', array());
  }

  /**
   * Condition form.
   */
  public function condition_form($context) {
    $form = array();
    $defaults = $this->fetch_from_context($context, 'values');

    $form['cookie_name'] = array(
      '#title' => t('Cookie name'),
      '#type' => 'select',
      '#options' => drupal_map_assoc(variable_get('context_cookie', array())),
      '#description' => t('Select cookie to check'),
      '#default_value' => isset($defaults['cookie_name']) ? $defaults['cookie_name'] : TRUE,
    );

    $form['cookie_value'] = array(
      '#title' => t('Cookie value'),
      '#type' => 'textfield',
      '#description' => t('Write the cookie value to compare'),
      '#default_value' => isset($defaults['cookie_value']) ? $defaults['cookie_value'] : TRUE,
    );

    $form['cookie_equality'] = array(
      '#title' => t('Value equality?'),
      '#type' => 'radios',
      '#description' => t('Cookie conditions'),
      '#options' => array(
        'equal' => t('Equal'),
        'not-equal' => t('Not equal'),
        'set' => t('Set'),
        'not-set' => t('Not set'),
      ),
      '#default_value' => isset($defaults['cookie_equality']) ? $defaults['cookie_equality'] : 'equal',
    );

    return $form;
  }

  /**
   * Condition form submit handler.
   */
  public function condition_form_submit($values) {
    $values['cookie_value'] = isset($values['cookie_value']) ? $values['cookie_value'] : FALSE;
    return array(
      'cookie_name'  => $values['cookie_name'],
      'cookie_value' => $values['cookie_value'],
      'cookie_equality' => $values['cookie_equality'],
    );
  }

  /**
   * Check to see if the requested cookie keys are current available.
   */
  function execute(array $cookie) {
    foreach ($this->get_contexts() as $context) {
      $settings = $this->fetch_from_context($context, 'values');
      if (isset($cookie[$settings['cookie_name']])) {
        switch ($settings['cookie_equality']) {
          case 'equal':
            if ($cookie[$settings['cookie_name']] === $settings['cookie_value']) {
              $this->condition_met($context);
            }
            break;

          case 'not-equal':
            if ($cookie[$settings['cookie_name']] !== $settings['cookie_value']) {
              $this->condition_met($context);
            }
            break;

          case 'set':
            if (isset($cookie[$settings['cookie_name']])) {
              $this->condition_met($context);
            }
            break;
        }
      }

      elseif ($settings['cookie_equality'] == 'not-set') {
        if (!isset($cookie[$settings['cookie_name']])) {
          $this->condition_met($context);
        }
      }
    }
  }

}
