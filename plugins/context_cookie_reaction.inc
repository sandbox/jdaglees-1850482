<?php

/**
 * @file
 * Context redirect reaction plugin for Context API.
 */

/**
 * Expose $_COOKIE variables to Context.
 */
class ContextCookieReaction extends context_reaction {

  /**
   * Implements options_form().
   */
  public function options_form($context) {

    $defaults = $this->fetch_from_context($context);

    $form['name'] = array(
      '#title' => t('Cookie name'),
      '#type' => 'textfield',
      '#default_value' => isset($defaults['name']) ? $defaults['name'] : '',
    );
    $form['value'] = array(
      '#title' => t('Value'),
      '#type' => 'textfield',
      '#description' => t('(Optional)'),
      '#default_value' => isset($defaults['value']) ? $defaults['value'] : '',
    );
    $form['expire'] = array(
      '#title' => t('Expire'),
      '#type' => 'textfield',
      '#description' => t('Length of time before cookie expires. Any English textual datetime description. If set to 0, the cookie will expire at the end of the session (when the browser closes).'),
      '#default_value' => isset($defaults['expire']) ? $defaults['expire'] : 0,
    );
    $form['path'] = array(
      '#title' => t('Path'),
      '#type' => 'textfield',
      '#description' => t("The path on the server in which the cookie will be available on. If set to '/', the cookie will be available within the entire domain. If set to '/foo/', the cookie will only be available within the /foo/ directory and all sub-directories such as /foo/bar/ of domain. If left blank, the default value is the current directory that the cookie is being set in."),
      '#default_value' => isset($defaults['path']) ? $defaults['path'] : '',
    );
    $form['domain'] = array(
      '#title' => t('Domain'),
      '#type' => 'textfield',
      '#description' => t('The domain that the cookie is available to'),
      '#default_value' => isset($defaults['domain']) ? $defaults['domain'] : '',
    );
    $form['secure'] = array(
      '#title' => t('Secure'),
      '#type' => 'textfield',
      '#description' => t('Indicates that the cookie should only be transmitted over a secure HTTPS connection from the client.'),
      '#default_value' => isset($defaults['secure']) ? $defaults['secure'] : '',
    );
    $form['httponly'] = array(
      '#title' => t('HTTP Only'),
      '#type' => 'textfield',
      '#description' => t('When TRUE the cookie will be made accessible only through the HTTP protocol.'),
      '#default_value' => isset($defaults['httponly']) ? $defaults['httponly'] : '',
    );

    return $form;
  }

  /**
   * Implements execute().
   */
  public function execute() {
    $contexts = $this->get_contexts();

    foreach ($contexts as $context) {
      if (is_array($context->reactions[$this->plugin])) {
        if (extract($context->reactions[$this->plugin]) == 7) {
          $_COOKIE[$name] = $value;
          $expire = strtotime('+' . $expire);
          setrawcookie($name, rawurlencode($value), $expire, $path, $domain, $secure, $httponly);
        }
      }
    }
  }
}
