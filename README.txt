CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
This module exposes cookie ($_COOKIE) variables to Context. 
It works similarly to Context Session and also allows to check whether a cookie 
has a specific value.

REQUIREMENTS
------------
This module requires the following module:
 * Context and Context UI (https://drupal.org/project/context)

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.

CONFIGURATION
-------------

MAINTAINERS
-----------
Current maintainers:
 * Jamil Daglees (Daglees) - https://drupal.org/user/47179
This project has been sponsored by:
 * YAAB
  Digital media company YAAB wants to become the next big online news hub in 
  the Middle East. With a goal of engaging editors and readers from throughout 
  the region, the Amman, Jordan-based startup plans to build four unique media
  sites in 18 months. To meet tight launch deadlines, YAAB has chosen 
  Drupal and Acquia Cloud as its development platform.
